import React from 'react';
import styled from 'styled-components';
import {Close} from '@styled-icons/evaicons-solid/Close';
import {UserPlus} from '@styled-icons/boxicons-regular/UserPlus'
import {Copy} from '@styled-icons/boxicons-regular/Copy'
import {Warning} from '@styled-icons/fluentui-system-filled/Warning'

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  vertical-align: middle;
`

const Card = styled.div`
  padding: 20px;
  width: 330px;
  height: 270px;
  font-family: Gill Sans, sans-serif;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.05), 0 0px 40px rgba(0, 0, 0, 0.08);
  border-radius: 7px;
`;

const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 15px;
`

const TextTitle = styled.p`
  font-weight: normal;
  font-size: 18px;
  margin: 0px;
`

const CloseCard = styled(Close)`
  color: black;
  cursor: pointer;
`

const Button = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #1775E7;
  border-radius: 4px;
  cursor: pointer;
  margin-bottom: 10px;
  padding-top: 3px;
  padding-bottom: 3px;
  border: none;
  &:hover {
      background-color: #1467CC;
  }
`

const IconButton = styled(UserPlus)`
  margin-right: 10px;
  color: white;
`
const TextButton = styled.p`
  margin: 5px 0px;
  color: white;
  font-weight: lighter;
`

const Text = styled.p`
  font-size: 15px;
  margin: 0px;
  padding-bottom: 10px;
  color: #5D5D5D;
`

const URL = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: #F2F3F5;
  border-radius: 4px;
  margin-bottom: 5px;
`

const TextUrl = styled.p`
  margin-left: 10px;
  text-align: justify;
  font-weight: normal;
`

const IconCopyUrl = styled(Copy)`
  margin-right: 10px;
  cursor: pointer;
  color: #616568;
`

const WarningContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-top: 5px;
`

const IconWarning = styled(Warning)`
  margin-right: 5px;
  color: #1775E7;
`

const TextWarning = styled.p`
  text-align: justify;
  margin: 0px;
  color: #5D5D5D;
  font-size: 14px;
`

const Email = styled.p`
  padding-top: 5px;
  margin: 0px;
  color: #5D5D5D;
  font-size: 14px;
`

const App = () => {
    return (
        <Container>
            <Card>
                <Title>
                    <TextTitle>Your meeting's ready</TextTitle>
                    <CloseCard size="28"/>
                </Title>
                <Button>
                    <IconButton size="20"/> <TextButton>Add others</TextButton>
                </Button>
                <Text>
                    Or share this meeting link with others you want in the meeting
                </Text>
                <URL>
                    <TextUrl>meet.google.com/inn-axdd-khv</TextUrl>
                    <IconCopyUrl size="23"/>
                </URL>
                <WarningContainer>
                    <IconWarning size="50"/>
                    <TextWarning>People who use this meeting link must get your permission before they can
                        join</TextWarning>
                </WarningContainer>
                <Email>
                    Joined as njavilas2015@gmail.com
                </Email>
            </Card>
        </Container>

    );
};

export default App;